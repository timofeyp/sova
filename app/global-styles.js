import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  
  #body{
    width: 130px;
    height: 180px;
    background: #89B6F0;
    position: fixed; left: 130px;
    z-index: 2;
    border-radius: 65px / 90px;
  }

#head{
    content: "";
    width: 130px;
    height: 100px;
    background: #B3D2FB;
    position: fixed; left: 130px;
    z-index: 4;
    border-radius: 65px / 50px;
}

#stomach{
    width: 110px;
    height: 170px;
    background: #5F9BE8;
    position: fixed; left: 140px;
    z-index: 3;
    border-radius: 55px / 85px;
}

#left-wing{
    width: 60px;
    height: 100px;
    background: #B3D2FB;
    position: fixed; top: 145px; left: 120px;
    z-index: 2;
    border-radius: 30px / 50px;
    transform: rotate(15deg);
}

#right-wing{
    width: 60px;
    height: 100px;
    background: #B3D2FB;
    position: fixed; top: 145px; left: 210px;
    z-index: 2;
    border-radius: 30px / 50px;
    transform: rotate(165deg);
    — moz-transition: all 1s ease;
    — webkit-transition: all 1s ease;
    — o-transition: all 1s ease;
    transition: all 1s ease;
}
#right-wing:hover {
    background: #5F9BE8;
    box-shadow: 0 0 0 8px #5F9BE8;
    -webkit-transform: scale(0.85);
    -moz-transform: scale(0.85);
    -ms-transform: scale(0.85);
}

#left-eye{
    width: 60px;
    height: 60px;
    position: fixed;
    z-index: 3;
    background-color: white;
    border-radius: 50%;
    border: 4px solid black;
    left: 140px;
    top: 95px;
    z-index: 5;
    — moz-transition: all 1s ease;
    — webkit-transition: all 1s ease;
    — o-transition: all 1s ease;
    transition: all 1s ease;
}

#left-eye:hover {
    background:#53ea93;
    -webkit-transform: scale(0.85);
    -moz-transform: scale(0.85);
    -ms-transform: scale(0.85);
    transform: scale(0.85);    
    opacity: 2;
    -webkit-transition: -webkit-transform 0.4s, opacity 0.2s;
    -moz-transition: -moz-transform 0.4s, opacity 0.2s;
    transition: transform 0.4s, opacity 0.2s;
}

#right-eye{
    width: 60px;
    height: 60px;
    position: fixed;
    z-index: 3;
    background-color: white;
    border-radius: 50%;
    border: 4px solid black;
    left: 190px;
    top: 95px;
    z-index: 5;
}

#right-eye:hover {
    background:#53ea93;
    -webkit-transform: scale(0.85);
    -moz-transform: scale(0.85);
    -ms-transform: scale(0.85);
    transform: scale(0.85);    
    opacity: 2;
    -webkit-transition: -webkit-transform 0.4s, opacity 0.2s;
    -moz-transition: -moz-transform 0.4s, opacity 0.2s;
    transition: transform 0.4s, opacity 0.2s;
}

#left-pupil{
    width: 30px;
    height: 30px;
    position: fixed;
    background-color: white;
    border-radius: 50%;
    border: 12px solid black;
    left: 155px;
    top: 110px;
    z-index: 6;
}

#right-pupil{
    width: 30px;
    height: 30px;
    position: fixed;
    background-color: white;
    border-radius: 50%;
    border: 12px solid black;
    left: 205px;
    top: 110px;
    z-index: 6;
}


#left-leg1 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 170px;
   top: 245px;
   z-index: 6;
   transform: rotate(8deg);
}

#left-leg2 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 174px;
   top: 246px;
   z-index: 6;
   transform: rotate(4deg);
}

#left-leg3 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 178px;
   top: 247px;
   z-index: 6;   
   
}

#right-leg1 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 215px;
   top: 245px;
   z-index: 6;
   transform: rotate(-8deg);
}

#right-leg2 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 211px;
   top: 246px;
   z-index: 6;
   transform: rotate(-4deg);
}

#right-leg3 {
   width: 6px;
   height: 12px;
   background-color: brown;
   -webkit-border-radius: 63px 63px 63px 63px / 108px 108px 72px 72px;
   border-radius:         50%  50%  50%  50%  / 60%   60%   40%  40%;
   position: fixed;
   left: 207px;
   top: 247px;
   z-index: 6;
}

#beak {
	width: 0;
	height: 0;
	border: 10px solid transparent;
	border-bottom: 10px solid #FFFEB3;
  position: fixed; top: 140px; left: 185px;
  z-index: 7;
	;
}

#beak:after {
	content: '';
  position: absolute; 
  z-index: 7;
	left: -10px; top: 10px;
	width: 0;
	height: 0;
	border: 10px solid transparent;
	border-top: 20px solid #FFFEB3;
}

#left-ear {
	width: 0;
	height: 0;
	border: 40px solid transparent;
	border-bottom: 130px solid #B3D2FB;
  position: fixed; 
  z-index: 1;
	left: 100px; top: -10px;
	transform: rotate(-15deg);
}

#left-ear:after {
	content: '';
	position: absolute;
	left: -60px; top: 70px;
	transform: rotate(-45deg);
	z-index: 1;
	width: 0;
	height: 0;
	border: 50px solid transparent;
	border-top: 70px solid #fafafa;
}

#right-ear {
	width: 0;
	height: 0;
	border: 40px solid transparent;
	border-bottom: 130px solid #B3D2FB;
  position: fixed; 
  z-index: 1;
	left: 210px; top: -10px;
	transform: rotate(15deg);
}

#right-ear:after {
	content: '';
	position: absolute;
	left: -40px; top: 70px;
	transform: rotate(45deg);
	width: 0;
	height: 0;
	border: 50px solid transparent;
	border-top: 70px solid #fafafa;
}

#right-ear-element  {
  width: 6px;
	height: 10px;
	background: #FFFEB3;
	position: fixed; 
  z-index: 10;
	left: 245px; top: 75px;
	transform: rotate(15deg);
}

#left-ear-element  {
  width: 6px;
	height: 10px;
	background: #FFFEB3;
	position: fixed; 
  z-index: 10;
	left: 140px; top: 75px;
	transform: rotate(-15deg);
}

#wool1{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 160px;
    top: 190px;
    z-index: 5;
}
#wool2{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 225px;
    top: 190px;
    z-index: 5;
}

#wool3{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 210px;
    top: 188px;
    z-index: 5;
}

#wool4{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 175px;
    top: 188px;
    z-index: 5;
}

#wool5{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 210px;
    top: 210px;
    z-index: 5;
}

#wool6{
    width: 8px;
    height: 8px;
    position: fixed;
    z-index: 10;
    border-radius: 50%;
    background-color: #FFFEB3;
    left: 175px;
    top: 210px;
    z-index: 5;
}

`;
