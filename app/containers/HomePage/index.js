/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  render() {
    return (
      <div>
        <br />
        <br />
        <br />
        <div id="body" />
        <div id="stomach" />
        <div id="left-wing" />
        <div id="right-wing" />
        <div id="head" />
        <div id="left-eye" />
        <div id="right-eye" />
        <div id="left-pupil" />
        <div id="right-pupil" />
        <div id="left-leg1" />
        <div id="left-leg2" />
        <div id="left-leg3" />
        <div id="right-leg1" />
        <div id="right-leg2" />
        <div id="right-leg3" />
        <div id="beak" />
        <div id="left-ear" />
        <div id="right-ear" />
        <div id="left-ear-element" />
        <div id="right-ear-element" />
        <div id="wool1" />
        <div id="wool2" />
        <div id="wool3" />
        <div id="wool4" />
        <div id="wool5" />
        <div id="wool6" />
      </div>
    );
  }
}
